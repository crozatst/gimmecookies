// cookies
let userid
let secret

// Cookie setting parameters
const maxAge = ';max-age='.concat(3600 * 24)
const sameSite = ';samesite=lax'
const path = ';path=/'
const third1 = 'utc.fr'
const third2 = 'pic.crzt.fr'
const ifrmurl = 'https://'.concat(third2).concat('/cookie/third2/frame.php')

function setId() {
  loadCookies()
  if (! userid) {
    console.log('settin cookie')
    const id = 'friend'.concat(Math.round(Math.random() * 1000))
    document.cookie = 'userid='.concat(id).concat(maxAge).concat(sameSite).concat(path)
  }
}

function loadCookies() {
  const cookies = decodeURIComponent(document.cookie).split(';')
  console.log(cookies)
  for (cookie of cookies) {
    const keyValue = cookie.split('=')
    if (keyValue[0].trim() === 'userid')
      userid = keyValue[1]
    else if (keyValue[0].trim() === 'secret')
      secret = keyValue[1]
  }
}

function displayId(style) {
  loadCookies()
  const p = document.querySelector('#userid')
  if (userid) {
    p.innerText = 'You are '
      .concat(' ', userid)
      .concat('.')
  }
  else {
    p.innerText = 'I was lying, I don\'t know who you are.'
  }
}

function recordSecret () {
  const secret = document.querySelector('#secret')
  const gotocookie = document.querySelector('#gotocookie')
  document.cookie = 'secret='.concat(secret.value).concat(maxAge).concat(sameSite).concat(path)
  if (document.querySelector('#third')) {
    window.location.reload(true)
  }
  gotocookie.hidden = false
}

function displaySecret() {
  const p = document.querySelector('#secret')
  if (secret) {
    p.innerText = 'You secret is :'
    p.innerText = p.innerText.concat(' ', secret)
  }
  else {
    p.innerText = 'I was lying, I don\'t know your secret.'
  }
}

function addIFrame() {
  const ifrm = document.createElement("iframe")
  ifrm.setAttribute("id", "third")
  let src = ifrmurl.concat('?userid=').concat(userid)
  if(secret) {
    src = src.concat('&secret=').concat(encodeURIComponent(secret))
  }
  ifrm.setAttribute("src", src)
  ifrm.style.width = "500px"
  ifrm.style.height = "180px"
  document.body.appendChild(ifrm)
}
