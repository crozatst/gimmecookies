<?php
$userid_1st = $_GET['userid'];
$secret = $_GET['secret'];
$userid_3rd = $_COOKIE['userid_3rd'];

// If user hasn't yet a cookie with userid_3rd, create one
if (! $userid_3rd) {
  $userid_3rd = "foaf".rand(10000, 99999);
}

setcookie("userid_3rd", $userid_3rd, [
  "expires" => time() + 3600 * 24,
  "path" => "/",
  "secure" => true,
  "samesite" => "None"
]);
setcookie("userid_1st", $userid_1st, [
  "expires" => time() + 3600 * 24,
  "path" => "/",
  "secure" => true,
  "samesite" => "None"
]);
setcookie("secret_1st", $secret, [
  "expires" => time() + 3600 * 24,
  "path" => "/",
  "secure" => true,
  "samesite" => "None"
]);
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Cookie testing</title>
  </head>
  <body>
    <p><b>I'am a cool distant content on <u>pic.crzt.fr</u> !</b></p>
    <p>Hope you don't mind I use cookies ?</p>
    <ul>
    <?php
      echo "<li>In my database you're identified as <code>".$userid_3rd."</code>.</li>";
    	echo "<li>I also know you in my partner base as <code>".$userid_1st."</code>.</li>";
    	if ($secret) {
    	   echo "<li>My partner is nice, he told me your secret : « ".$secret." ».</li>";
	    }
    ?>
    </ul>
  </body>
</html>
