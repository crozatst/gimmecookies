<?php
$userid_1st = $_COOKIE['userid_1st'];
$secret = $_COOKIE['secret_1st'];
$userid_3rd = $_COOKIE['userid_3rd'];

// If user hasn't yet a cookie with userid_3rd, create one
if (! $userid_3rd) {
  $userid_3rd = "foaf".rand(10000, 99999);
}

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Cookie testing</title>
  </head>
  <body>

    <ul>
    <?php
      echo "<li>At pic.crzt.fr, you're identified as <code>".$userid_3rd."</code>.</li>";
      if ($userid_1st) {
        echo "<li>At utc.fr, you're identified as <code>".$userid_1st."</code>.</li>";
      }
    	if ($secret) {
    	   echo "<li>Your secrets are : « ".$secret." ».</li>";
	    }
    ?>
    </ul>
  </body>
</html>
