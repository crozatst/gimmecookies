<?php
$userid_1st = $_COOKIE['userid_1st'];
$secret = $_COOKIE['secret_1st'];
$userid_3rd = $_COOKIE['userid_3rd'];

if (! $userid_3rd) {
  $userid_3rd = "foaf".rand(10000, 99999);
}

setcookie("userid_3rd", $userid_3rd, [
  "expires" => time() + 3600 * 24,
  "path" => "/",
  "secure" => true,
  "samesite" => "None"
]);
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Cookie testing</title>
  </head>
  <body>
    <?php
      echo "<p>Hi, <code>".$userid_3rd."</code>, how are you ?</p>";
    	if ($userid_1st) {
        echo "<p>In my partner base I know you as <code>".$userid_1st."</code>.</p>";
      }
    	if ($secret) {
    	   echo "<p>My partner is nice, he told me your secret : « ".$secret." ».</p>";
	    }
    ?>
    <p>Press F5 to check if I learned new things...</p>
    <p>By the way, you should visit also <a href="https://punkardie.fr/cookie">https://punkardie.fr/cookie</a></p>
  </body>
</html>
